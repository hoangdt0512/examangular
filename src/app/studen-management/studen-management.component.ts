import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {StudentService} from '../student.service';
import {Observable} from 'rxjs';
import {Student} from '../shared/student';

@Component({
  selector: 'app-studen-management',
  templateUrl: './studen-management.component.html',
  styleUrls: ['./studen-management.component.css']
})
export class StudenManagementComponent implements OnInit {
  students: Observable<Student[]>;
  dataSaved: boolean;
  studentForm: any;
  studentIdUpdate = null;
  message = null;
  constructor(
    private formBulder: FormBuilder,
    private studentService: StudentService) {
  }

  ngOnInit(): void {
    // @ts-ignore
    this.studentForm = this.formBulder.group(
      {
        name: ['', [Validators.required]],
        address: ['', [Validators.required]],
        dateOfJoin: ['', [Validators.required]],
      }
    );
    this.loadStudents();
  }

  // tslint:disable-next-line:typedef
  loadStudents() {
    this.students = this.studentService.getAllStudents();
  }

  // tslint:disable-next-line:typedef
  onFormSubmit() {
    this.dataSaved = false;
    const student = this.studentForm.value;
    if (this.studentIdUpdate){
      this.updateStudent(student);
    }else {
      this.createStudent(student);
    }
  }
  // tslint:disable-next-line:typedef
  createStudent(student: Student) {
    if (this.studentIdUpdate == null) {
      this.studentService.createStudent(student).subscribe(
        () => {
          this.dataSaved = true;
          this.message = 'Record saved Successfully';
          this.loadStudents();
          this.studentIdUpdate = null;
          this.studentForm.reset();
        }
      );
    }
  }
  // tslint:disable-next-line:typedef
  loadStudentToEdit(id: string) {
    // tslint:disable-next-line:no-shadowed-variable
    this.studentService.getStudentById(id).subscribe(Student => {
      this.message = null;
      this.dataSaved = false;
      this.studentIdUpdate = Student.id;
      this.studentForm.controls.name.setValue(Student.name);
      this.studentForm.controls.address.setValue(Student.address);
      this.studentForm.controls.dateOfJoin.setValue(Student.dateOfJoin);
    });

  }
  // tslint:disable-next-line:typedef
  updateStudent(student: Student){
    if (this.studentIdUpdate != null) {
      student.id = this.studentIdUpdate;
      this.studentService.updateStudent(student).subscribe(() => {
        this.dataSaved = true;
        this.message = 'Record Updated Successfully';
        this.loadStudents();
        this.studentIdUpdate = null;
        this.studentForm.reset();
      });
    }
  }
  // tslint:disable-next-line:typedef
  deleteStudent(id: string) {
    if (confirm('Are you sure you want to delete this ?')) {
      this.studentService.deleteStudentByID(id).subscribe(() => {
        this.dataSaved = true;
        this.message = 'Record Deleted Succefully';
        this.loadStudents();
        this.studentIdUpdate = null;
        this.studentForm.reset();
      });
    }
  }
}
