import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudenManagementComponent } from './studen-management.component';

describe('StudenManagementComponent', () => {
  let component: StudenManagementComponent;
  let fixture: ComponentFixture<StudenManagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StudenManagementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StudenManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
