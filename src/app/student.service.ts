import {Injectable} from '@angular/core';
import {HttpClient , HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Student} from './shared/student';

@Injectable({
  providedIn : 'root'
})
export class StudentService {
  url = 'http://thtam.local/api/users';
  constructor(private http: HttpClient) {
  }
  getAllStudents(): Observable<any>{
    return this.http.get(this.url);
  }
  // tslint:disable-next-line:typedef
  getStudentById(id: string){
    return this.http.get<Student>(this.url + '/' + id);
  }
  createStudent(student: Student): Observable<Student>{
    const httpOptions = { headers : new HttpHeaders({'Content-Type': 'application/json'})};
    return this.http.post<Student>(this.url, student, httpOptions);
  }
  // @ts-ignore
  updateStudent(student: Student): Observable<Student>{
    const httpOptions = { headers : new HttpHeaders({'Content-Type': 'application/json'})};
    // @ts-ignore
    return this.http.put(this.url + '/' + student.id, student , httpOptions);
  }
  deleteStudentByID(id: string): Observable<number>{
    const httpOptions = { headers : new HttpHeaders({'Content-Type': 'application/json'})};
    return this.http.delete<number>(this.url + '/' + id, httpOptions);
  }
}
