import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StudenManagementComponent } from './studen-management/studen-management.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {StudentService} from './student.service';

@NgModule({
  declarations: [
    AppComponent,
    StudenManagementComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule

  ],
  providers: [StudentService],
  bootstrap: [AppComponent]
})
export class AppModule { }
