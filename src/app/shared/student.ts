export class Student {
    id: string;
    name: string;
    address: string;
    dateOfJoin: Date;
}
